from django.db import models


class Vkpost(models.Model):
    idpost = models.CharField(max_length = 15)
    date = models.DateTimeField()
    groupid = models.CharField(max_length = 15)
    urls = models.TextField()
    photos = models.TextField()
    textpost = models.TextField()

    def __str__(self):
        return self.urls

class Accaunt(models.Model):
        login = models.CharField(max_length = 15)
        passwd = models.CharField(max_length = 15)
        site = models.CharField(max_length = 5)

from django.core.files.storage import FileSystemStorage

fs = FileSystemStorage(location='media/photos')

class ModelFormWithFileField(models.Model):
        name = models.CharField(max_length = 20)
        photo = models.ImageField(storage=fs)
