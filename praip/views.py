from django.shortcuts import render
import vk_api
from django.http import HttpResponseRedirect

from .form import UploadFileForm
from .models import ModelFormWithFileField
import json

global vk_session


def index(request):
    get_userid()
    return render(request, 'praip/index.html')
# Create your views here.

def captcha_handler(captcha):
    key = input("Enter captcha code {0}: ".format(captcha.get_url())).strip()
    return captcha.try_again(key)

def get_userid():
    global vk_session
    vk = vk_session.get_api()
    r = vk.photos.getAll(count="1")
    item = str(r['items'])
    item = item[1:(len(item)-1):1]
    print(item)
    dict(item)
    print(item["owner_id"])

def login(request):
    if request.method == "POST":
        login = request.POST['login']
        password = request.POST['passwd']
        global vk_session
        vk_session = vk_api.VkApi(login, password, captcha_handler=captcha_handler)
        try:
            vk_session.auth()
        except vk_api.AuthError as error_msg:
            print(error_msg)
            return render(request, 'praip/loginvk.html')
        return render(request, 'praip/index.html')
    else:

        return render(request, 'praip/loginvk.html',)

def uploadphoto(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            instance = ModelFormWithFileField(photo=request.FILES['file'])
            instance.save()
            return HttpResponseRedirect('')
    else:
        form = UploadFileForm()
    return render(request, 'praip/uploadphoto.html', {'form': form})

def postvk(request):
    upload = vk_api.VkUpload(vk_session)

    photo = upload.photo(  # Подставьте свои данные
        'media/photos/1.jpg',
        album_id=251834237
    )

    vk_photo_url = 'https://vk.com/photo{}_{}'.format(
        photo[0]['owner_id'], photo[0]['id']
    )


    print(photo, '\nLink: ', vk_photo_url)
    if request.method == "POST":
        group = "-"+request.POST['group']
        print(group)
        message = request.POST['message']
        print(message)
        global vk_session
        vk = vk_session.get_api()
        r = vk.wall.post(owner_id=group, message=message, attachments='photo{}_{}'.format(photo[0]['owner_id'], photo[0]['id']
    ))
        return render(request, 'praip/index.html')
    else:
        return render(request, 'praip/postvk.html')
    return render(request, 'praip/index.html')
